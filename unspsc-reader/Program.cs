﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace unspsc_reader
{
    class Program
    {



       

        static void Main(string[] args)
        {
            List<Item> items = ReadCsvFile();

            List<Item> distinctCodes = DistinctCodes(items);

            List<string> dbItems = DatabaseItems();

      


            var leftJoinded = from dbItem in dbItems
                join distinctCode in distinctCodes
                on dbItem equals distinctCode.Name
                into temp
                from item in temp.DefaultIfEmpty()
                select new
                {
                    Name = dbItem,
                    Code = item != null ? item.Code : null
                };

            var list = leftJoinded.ToList();






            Console.Read();



        }

        private static List<string> DatabaseItems()
        {
            List<string> itemList = new List<string>();
            string line;
            //int counter = 0;
            System.IO.StreamReader file = new System.IO.StreamReader(@"./csv/databaseitem.txt");
            while ((line = file.ReadLine()) != null)
            {
                itemList.Add(line);
            }

            return itemList;
        }


        private static List<Item> DistinctCodes(List<Item> items)
        {
            List<Item> distList = items.ToLookup(o => o.Code).Select(o => o.First()).ToList();
            return distList;
        }

        private static List<Item> ReadCsvFile()
        {
            List<Item> items = new List<Item>();

            string line;
            int counter = 0;
            System.IO.StreamReader file = new System.IO.StreamReader(@"./csv/unspsc.csv");
            while ((line = file.ReadLine()) != null)
            {

                if (counter > 0)
                {
                    string[] strings = line.Split(',');

                    items.Add(new Item() { Code = strings[0], Name = strings[1]});
                    items.Add(new Item() { Code = strings[2], Name = strings[3]});
                    items.Add(new Item() { Code = strings[4], Name = strings[5]});
                    items.Add(new Item() { Code = strings[6], Name = strings[7]});
                }


                //if (counter == 10)
                //{
                //    break;
                    
                //}

                counter++;
            }




            file.Close();
            return items;
        }
    }
}

public class Item
{
    public string Code { get; set; }
    public string  Name { get; set; }


}



